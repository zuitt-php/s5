<?php

$tasks = ['Get git', 'Bake HTML', 'Eat CSS', 'Learn PHP'];

//isset() checks the presence of the given global variable $_GET['index']. If the global variable exists, it will render true, if not, it will render false)

if(isset($_GET['index'])){

    $indexGet = $_GET['index'];
    echo "The retreived task from GET is $tasks[$indexGet]";
}

if(isset($_POST['index'])){

    $indexPost = $_POST['index'];
    echo "The retreived task from POST is $tasks[$indexPost]";
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PHP SC S5</title>
</head>
<body>
        <h1>Task index from GET</h1>

        <form method="GET">

            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>

        </form>

        <h1>Task index from POST</h1>

        <form method="POST">

        <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
        </select>

            <button type="submit">POST</button>

        </form>
</body>
</html>