<?php

//reserved function
session_start();

class TaskList{
	public function add($description){
		$newTask = (object)[
			'description' => $description,
			'isFinished' => false
		];

		if($_SESSION['tasks'] === null){
			$_SESSION['tasks'] = array();
		}

		array_push($_SESSION['tasks'], $newTask);
	}

	public function update($id, $description, $isFinished){
		$_SESSION['tasks'][$id]->description = $description;
		$_SESSION['tasks'][$id]->isFinished = ($isFinished !== null )? true : false;
	}

	public function remove($id){
		//splice(array, start(array index), how many(delete count))
		array_splice($_SESSION['tasks'], $id, 1);
	}
}

$taskList = new TaskList();

if($_POST['action'] === 'add'){
	$taskList->add($_POST['description']);
}else if($_POST['action'] === 'update'){
	$taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}else if($_POST['action'] === 'remove'){
	$taskList->remove($_POST['id']);
}

header('Location: ./index.php');

