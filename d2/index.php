<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S5</title>
</head>
<body>

	<?php session_start(); ?>
	
	<h1>Add Task</h1>
	<form method="POST" action="./server.php">
		<!-- the value of the "action" attribute dictates where the form data should go -->
		<input type="hidden" name="action" value="add">
		Description: <input type="text" name="description" required>
		<button type="submit">Add</button>
	</form>

	<h1>Task List</h1>

	<?php if(isset($_SESSION['tasks'])): ?>
		<?php foreach($_SESSION['tasks'] as $index => $task): ?>

			<div>
				<form method="POST" action="./server.php" style="display: inline-block;">
				<input type="hidden" name="action" value="update">
				<input type="hidden" name="id" value="<?= $index;?>">
				<input type="checkbox" name="isFinished" <?= ($task->isFinished)? "checked" : null ?>>
				<input type="text" name="description" value="<?= $task->description; ?>">
				<button type="submit">Update</button>
				</form>

				<form method="POST" action="./server.php" style="display: inline-block;">
				<input type="hidden" name="action" value="remove">
				<input type="hidden" name="id" value="<?= $index;?>">
				<button type="submit">Delete</button>
				</form>
			</div>

		<?php endforeach; ?>
	<?php endif; ?>
</body>
</html>
