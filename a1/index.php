<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S5 A1</title>
</head>
<body>
	<?php session_start(); ?>

	<?php if(isset($_SESSION['emptyUser'])): ?>

	<h1>Login</h1>

	<form method="POST" action="./server.php">

		Username: <input type="email" name="email" required>
		Password: <input type="password" name="password" required>

		<button type="submit">Login</button>

	</form>

	<?php endif;?>

	<?php if(isset($_SESSION['loginUser'])): ?>

		<p>Login successful!</p>

		<p>Hello, <?= $_SESSION['loginUser']; ?></p>

		<form method="POST" action="./server.php">
			<input type="hidden" name="logout" value="logout">
			<button type="submit">Logout</button>
		</form>
		

	<?php elseif(isset($_SESSION['loginFail'])): ?>

		<p><?= $_SESSION['loginFail']; ?></p>

	<?php else: ?>

		<?php if(isset($_SESSION['loginInvalid'])): ?>

			<p><?= $_SESSION['loginInvalid']; ?></p>

		<?php endif;?>	

	<?php endif;?>
</body>
</html>
